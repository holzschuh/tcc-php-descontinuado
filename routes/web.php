<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::any('token', function () {
    return response()->json(md5(uniqid('C',false)), 200);
});

Route::group(['prefix' => 'json'], function () {
    Route::get('users', function() {
        return App\User::all();
    });
});

Route::resource('institutions', 'InstitutionController');

Route::resource('users', 'UserController');

Route::resource('events', 'EventController');

Route::resource('events/{id}/days', 'DayController');

Route::resource('events/{id}/lectures', 'LectureController');

Route::resource('events/{id}/workshops', 'WorkshopController');


Route::get('main', function() {
    return view('partials.welcome');
})->name('main');


Route::get('teste', function() {
    return view('partials.events.lectures.list', ['obj' => App\Event::first()]);
    return view('partials.workshops.add', ['institutions' => App\Institution::all()]);
});
