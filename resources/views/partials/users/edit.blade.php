@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Editar Usuário</h4>
                <!-- <p class="card-category">Edite o Usuário</p> -->
            </div>
            <div class="card-body">
                <form method="post" action="{{ action('UserController@update', $obj->id) }}">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="row">
                        @if ($errors->any())
                            <div class="col-12">
                            @foreach ($errors->all() as $error)
                                <p class="alert alert-danger">{{ $error }}</p>
                            @endforeach                    
                            </div>
                        @endif
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">ID</label>
                                <input type="text" class="form-control" disabled="" value="{{ $obj->id }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="Nome do usuário">
                                <label class="bmd-label-floating">Nome</label>
                                <input type="text" class="form-control" name="name" value="{{ $obj->name }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="Email do usuário">
                                <label class="bmd-label-floating">Email</label>
                                <input type="text" class="form-control" name="email" value="{{ $obj->email }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="CPF do usuário">
                                <label class="bmd-label-floating">CPF</label>
                                <input type="text" class="form-control" name="cpf" value="{{ $obj->cpf }}">
                            </div>
                        </div>
                        <select class="selectpicker col-md-6" data-style="select-with-transition" data-live-search="true" title="Instituição" name="institution">
                            <option disabled selected>Instituição</option>
                            @foreach($institutions as $i)
                            <option value="{{ $i->id }}" {{ $obj->institution ? ($obj->institution->id == $i->id ? 'selected' : '') : ''}}>{{ $i->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="BIO do usuário">
                                <label class="bmd-label-floating">Descrição/BIO</label>
                                <textarea class="form-control" rows="5" name="bio">{{ $obj->bio }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">Atualizar</button>
                    <a href="{{ action('UserController@index') }}" class="btn btn-secondary pull-right">Voltar</a>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection