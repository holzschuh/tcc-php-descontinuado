@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Adicionar Usuário</h4>
                <p class="card-category">Adicionar um novo usuário</p>
            </div>
            <div class="card-body">
                <form method="post" action="{{ action('UserController@store') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        @if ($errors->any())
                            <div class="col-12">
                            @foreach ($errors->all() as $error)
                                <p class="alert alert-danger">{{ $error }}</p>
                            @endforeach                    
                            </div>
                        @endif
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="Nome do usuário">
                                <label class="bmd-label-floating">Nome</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="Email do usuário">
                                <label class="bmd-label-floating">Email</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="CPF do usuário">
                                <label class="bmd-label-floating">CPF</label>
                                <input type="text" class="form-control" name="cpf" value="{{ old('cpf') }}">
                            </div>
                        </div>
                        <select class="selectpicker col-md-6" data-style="select-with-transition" data-live-search="true" title="Instituição" name="institution">
                            <option disabled selected>Instituição</option>
                            @foreach($institutions as $i)
                            <option value="{{ $i->id }}">{{ $i->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="BIO do usuário">
                                <label class="bmd-label-floating">Descrição/BIO</label>
                                <textarea class="form-control" rows="5" name="bio">{{ old('bio') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                    <a href="{{ route('main') }}" class="btn btn-secondary pull-right">Voltar</a>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection