@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Adicionar Instituição</h4>
                <p class="card-category">Adicionar uma nova instituição</p>
            </div>
            <div class="card-body">
                <form method="post" action="{{ action('InstitutionController@store') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        @if ($errors->any())
                            <div class="col-12">
                            @foreach ($errors->all() as $error)
                                <p class="alert alert-danger">{{ $error }}</p>
                            @endforeach                    
                            </div>
                        @endif
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group" rel="tooltip" data-original-title="Nome da instituição">
                                <label class="bmd-label-floating">Nome</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                    <a href="{{ route('main') }}" class="btn btn-secondary pull-right">Voltar</a>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection