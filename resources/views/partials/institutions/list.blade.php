@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex align-items-center"><i class="material-icons pr-1 mb-1">school</i>
                        <h4 class="card-title "> Instituições</h4>
                    </div>
                    <a href="{{ action('InstitutionController@create') }}" class="btn btn-round btn-warning"><i class="material-icons">add</i>Add
                        novo</a>
                </div>
                <!-- <h4 class="card-title ">Administradores <a href="#" class="btn btn-round btn-fab btn-">+</a></h4>
                      <p class="card-category d-none"> Here is a subtitle for this table</p> -->
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if(session()->has('msg'))
                        <p class="alert alert-{{ session('msg.type') }}">{{ session('msg.text') }}</p>                
                    @endif
                    <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($list as $o)
                                <tr>
                                    <td>{{$o->id}}</td>
                                    <td>{{$o->name}}</td>
                                    <td class="td-actions ">
                                        <a href="{{ action('InstitutionController@edit', $o->id) }}" rel="tooltip" title="" class="btn btn-primary btn-fab btn-round btn-sm" data-original-title="Editar">
                                            <i class="material-icons">edit</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                        <script>
                                            var abre = function () {
                                                swal({
                                                    title: "Feito!",
                                                    text: "Administrador deletado com sucesso!",
                                                    buttonsStyling: false,
                                                    confirmButtonClass: "btn btn-success",
                                                    type: "success"
                                                }).catch(swal.noop)
    
                                            }
                                        </script>
                                        <form action="{{ action('InstitutionController@destroy', $o->id) }}" method="post" class="d-inline">
                                            {{csrf_field()}}
                                            {{method_field('delete')}}
                                        <button onClick="abre()" type="submit" rel="tooltip" title="" class="btn btn-danger btn-fab btn-round btn-sm"
                                            data-original-title="Remover">
                                            <i class="material-icons">close</i>
                                        </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <p>No users</p>
                            @endforelse                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection