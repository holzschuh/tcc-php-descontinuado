@extends('layouts.main')

@section('content')
<h1 class="display-2">Olá, {{ Auth::user()->name }}</h1>
<h2 class="display-4 mt-3">Selecione uma das opções do menu para começar</h2>
@endsection