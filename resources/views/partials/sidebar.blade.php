<ul class="nav">
    <li class="nav-item {{ request()->routeIs('dash*') ? 'active' : ''}}">
        <a class="nav-link" href="/main">
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
        </a>
    </li>
    <li class="nav-item {{ request()->routeIs('users*') ? 'active' : ''}}">
        <a class="nav-link" href="{{ action('UserController@index') }}">
            <i class="material-icons">person</i>
            <p>Usuários</p>
        </a>
    </li>
<li class="nav-item {{ request()->routeIs('events*') ? 'active' : ''}}">
        <a class="nav-link" href="{{ action('EventController@index') }}">
            <i class="material-icons">event</i>
            <p>Eventos</p>
        </a>
    </li>
    <li class="nav-item {{ request()->routeIs('institutions*') ? 'active' : ''}}">
        <a class="nav-link" href="{{ action('InstitutionController@index') }}">
            <i class="material-icons">school</i>
            <p>Instituições</p>
        </a>
    </li>
    <li class="d-none nav-item {{ request()->routeIs('conf*') ? 'active' : ''}}">
        <a class="nav-link" href="./settings.html">
            <i class="material-icons">settings</i>
            <p>Configurações</p>
        </a>
    </li>
    <li class="separator"></li>
</ul>