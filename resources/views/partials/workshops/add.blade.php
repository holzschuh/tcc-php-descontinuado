@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Adicionar Oficina</h4>
                <!-- <p class="card-category">Entre com as informações do usuário</p> -->
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Evento</label>
                                <input type="text" class="form-control" disabled="" value="NOme do evento">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Titulo</label>
                                <input name="title" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Local</label>
                                <input type="email" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Limite de ouvintes</label>
                                <input type="number" min="0" name=""  class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Confirmada</label>
                                <input type="checkbox" value="0" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <select class="selectpicker col-md-4" data-style="select-with-transition" data-live-search="true"
                            title="Dia" name="day">
                            <option disabled selected>Dia</option>
                            @foreach($institutions as $i)
                            <option value="{{ $i->id }}">{{ $i->name }}</option>
                            @endforeach
                        </select>
                        <select class="selectpicker col-md-8" data-style="select-with-transition" data-live-search="true"
                            title="Palestrantes" name="speakers">
                            <option disabled selected>Palestrantes</option>
                            @foreach($institutions as $i)
                            <option value="{{ $i->id }}">{{ $i->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Adicionar</button>
                    <button type="submit" class="btn btn-secondary pull-right">Voltar</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection