@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Adicionar Evento</h4>
                <p class="card-category">Entre com as informações do evento</p>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ action('EventController@store') }}">
                    {{ csrf_field() }}
                        <div class="row">
                        @if ($errors->any())
                            <div class="col-12">
                            @foreach ($errors->all() as $error)
                                <p class="alert alert-danger">{{ $error }}</p>
                            @endforeach                    
                            </div>
                        @endif
                        <div class="col-md-12">
                            <h4><strong>Informações básicas</strong></h4>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Nome do evento</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Edição do evento</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group">
                                <label class="label-control bmd-label-static">Data de início</label>
                                <input type="date" class="form-control datetimepicker" name="datetime_start">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group">
                                <label class="label-control bmd-label-static">Data de encerramento</label>
                                <input type="date" class="form-control datetimepicker" name="datetime_end">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Local</label>
                                <input type="text" class="form-control" name="location">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 mt-4">
                            <h4><strong>Imagens</strong></h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group-file" rel="tooltip" title="" data-original-title="Esta imagem irá aparecer na página do evento">
                                <label for="logo">Logo (pref. AAAxBBB)</label>
                                <input type="file" name="" class="form-control" id="logo">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group-file" rel="tooltip" title="" data-original-title="Esta imagem irá aparecer na página de listagem de eventos">
                                <label for="thumb">Thumbnail (pref. AAAxBBB)</label>
                                <input type="file" name="" class="form-control" id="thumb">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group-file">
                                <label for="ba">Banner A (pref. AAAxBBB)</label>
                                <input type="file" name="" class="form-control" id="ba">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group-file">
                                <label for="ba">Banner B (pref. AAAxBBB)</label>
                                <input type="file" name="" class="form-control" id="ba">
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-group form-file-upload form-file-multiple">
                    <input type="file" multiple="" class="inputFileHidden">
                    <div class="input-group">
                        <input type="text" class="form-control inputFileVisible" placeholder="Single File">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-fab btn-round btn-primary">
                                <i class="material-icons">attach_file</i>
                            </button>
                        </span>
                    </div>
                  </div>   -->

                    <div class="row d-none">
                        <div class="col-md-12 mt-4">
                            <h4><strong>Configurações</strong></h4>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">URL para o evento:</label>
                                <input type="url" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row d-none">
                        <div class="col-md-12 mt-2">
                            <h4><strong>Personalização de cores</strong></h4>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group is-filled"><label for="primary" class="bmd-label-static">Primary</label><input
                                    type="color" name="primary" id="primary" class="form-control"></div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group is-filled"><label for="secondary" class="bmd-label-static">Secondary</label><input
                                    type="color" name="secondary" id="secondary" class="form-control"></div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group is-filled"><label for="info" class="bmd-label-static">Info</label><input
                                    type="color" name="info" id="info" class="form-control"></div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group is-filled"><label for="warning" class="bmd-label-static">Warning</label><input
                                    type="color" name="warning" id="warning" class="form-control"></div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group is-filled"><label for="success" class="bmd-label-static">Success</label><input
                                    type="color" name="success" id="success" class="form-control"></div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group is-filled"><label for="danger" class="bmd-label-static">Danger</label><input
                                    type="color" name="danger" id="danger" class="form-control"></div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Adicionar</button>
                    <button type="submit" class="btn btn-secondary pull-right">Voltar</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection