@extends('partials.events.base')

@section('econtent')
<div class="row">

  <div class="col-md-12">
    <div class="col-md-12 d-flex justify-content-end align-items-center">
      <div class="d-none">
        <h4><strong>Dias</strong></h4>
      </div>
      <div><a href="{{ action('DayController@create', $event->id) }}" class="btn btn-primary btn-sm btn-round" rel="tooltip" data-original-title="Adicionar nova dia"><i
            class="material-icons">add</i>
          Incluir</a></div>
    </div>
    <div class="col-md-12">
      @if(session()->has('msg'))
        <p class="alert alert-{{ session('msg.type') }}">{{ session('msg.text') }}</p>                
      @endif
      <div class="row">

      @forelse ($list as $key=>$o)
        <div class="col-xl-3 col-md-4 col-sm-6">
          <div class="card " >
            <div class="card-body">
              <h3 class="card-title">DIA {{ $key+1 }}</h3>
              <h6 class="card-subtitle mt-1 mb-2 text-muted">{{ $o->date }}</h6>
              <hr>
              <div class="float-left">
                <span class="badge badge-secondary p-1"></span>
              </div>
              <div class="d-flex justify-content-end">
                <a href="#" class="pl-2 pr-2 btn btn-link text-muted" rel="tooltip" data-original-title="Ver detalhes"><i class="material-icons">visibility</i></a>
                <form action="{{ action('DayController@destroy', [$event->id, $o]) }}" method="post">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <button type="submit" class="pl-2 pr-2 btn btn-link text-muted" rel="tooltip" data-original-title="Remover dia"><i class="material-icons">delete</i></button>
                </form>
              </div>
            </div>
          </div>
        </div>
      @empty
        <div class="pl-4 pb-4">
          <h2>Ainda não há dias cadastrados</h2>
        </div>
      @endforelse
      </div>
    </div>
  </div>

  <div class="col-md-6 d-none">
    <div class="col-md-12 d-flex justify-content-between align-items-center">
      <div>
        <h4><strong>Oficinas</strong></h4>
      </div>
      <div><a href="#" class="btn btn-primary btn-sm btn-round" rel="tooltip" data-original-title="Adicionar nova oficina"><i
            class="material-icons">add</i>
          Incluir</a></div>
    </div>
    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr>
            <th>Título</th>
            <th>Status</th>
            <th class="th-action"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><small>Lorem ipsum sum da tret bisect growes upl layer</small></td>
            <td><span class="badge badge-success">CONFIRMADO</span></td>
            <td class="td-action text-right">
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Ver oficina"><i
                  class="material-icons">visibility</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Editar oficina"><i
                  class="material-icons">edit</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Remover oficina"><i
                  class="material-icons">close</i>
                <div class="ripple-container"></div>
              </a>
            </td>
          </tr>
          <tr>
            <td><small>Lorem ipsum sum da tret bisect growes upl layer</small></td>
            <td><span class="badge badge-warning">PENDENTE</span></td>
            <td class="td-action text-right">
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Ver oficina"><i
                  class="material-icons">visibility</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Editar oficina"><i
                  class="material-icons">edit</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Remover oficina"><i
                  class="material-icons">close</i>
                <div class="ripple-container"></div>
              </a>
            </td>
          </tr>
          <tr>
            <td><small>Lorem ipsum sum da tret bisect growes upl layer</small></td>
            <td><span class="badge badge-danger">CANCELADO</span></td>
            <td class="td-action text-right">
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Ver oficina"><i
                  class="material-icons">visibility</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Editar oficina"><i
                  class="material-icons">edit</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Remover oficina"><i
                  class="material-icons">close</i>
                <div class="ripple-container"></div>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

</div>
@endsection