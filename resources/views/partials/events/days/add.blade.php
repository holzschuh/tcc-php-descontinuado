@extends('partials.events.base')

@section('econtent')
<div class="row">
  <div class="col-md-12 pl-4 pr-4 mt-3">
    <form method="POST" action="{{ action('DayController@store', $event->id) }}">
      {{ csrf_field() }}
      <div class="row">

        @if ($errors->any())
          <div class="col-12">
          @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
          @endforeach                    
          </div>
        @endif

        <div class="col-md-12">
            <div class="form-group bmd-form-group">
                <label class="label-control bmd-label-static">Data do dia</label>
                <input type="date" class="form-control datetimepicker" name="datetime_day">
                <input type="hidden" name="event" value="{{ $event->id }}">
            </div>
        </div>
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary pull-right">Adicionar</button>
          <a href="{{ action('DayController@index', $event->id) }}" class="btn btn-secondary pull-right">Voltar</a>
        </div>

      </div>
    </form>      
  </div>
</div>
@endsection