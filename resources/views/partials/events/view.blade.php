@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!-- <div class="card-header card-header-primary">
                <h4 class="card-title">NOME DO EVENTO</h4>
              </div> -->
            <div class="card-body">
               

                    <div class="row mt-1 mb-3">
                        <div class="col-md-12 d-flex align-items-center justify-content-between">
                            <h2><i class="material-icons mr-2">event</i><strong>{{$obj->name}}</strong></h2>
                            <div>
                            <a href="{{ action('EventController@edit', $obj->id) }}" rel="tooltip" title="" class="btn btn-primary btn-fab btn-round btn-sm" data-original-title="Editar evento"><i class="material-icons">edit</i><div class="ripple-container"></div></a>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row">
                        <div class="col">
                            <hr>
                        </div>
                    </div> --}}
                    
                    {{-- <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <div>
                                <h4><strong>DIAS</strong></h4>
                            </div>
                            <div><a href="#" class="btn btn-primary btn-sm btn-round" rel="tooltip"
                                    data-original-title="Adicionar novo dia"><i class="material-icons">add</i>
                                    Adicionar</a></div>
                        </div>
                    </div> --}}

                    <div class="row">
                        <div class="col-md-12">
                            <!-- {{ $obj }} -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        <a href="{{ action('DayController@index', $obj->id) }}" class="btn btn-primary btn-round btn-block">Acessar Dias</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        <a href="{{ action('LectureController@index', $obj->id) }}" class="btn btn-primary btn-round btn-block">Acessar Palestras</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <a href="{{ action('WorkshopController@index', $obj->id) }}" class="btn btn-primary btn-round btn-block">Acessar Oficinas</a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection