@extends('partials.events.base')

@section('econtent')
<hr>
<div class="row">
  <div class="col-md-12 pl-4 pr-4">
    <form method="POST" action="{{ action('LectureController@store', $event->id) }}">
      {{ csrf_field() }}
      <div class="row">

        @if ($errors->any())
          <div class="col-12">
          @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
              </button>
              <span>{{ $error }}</span>
            </div>
          @endforeach                    
          </div>
        @endif

        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nome da palestra</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Local</label>
                <input type="text" class="form-control" name="location" value="{{ old('location') }}">
            </div>
        </div>
        <select class="selectpicker col-md-6" data-style="select-with-transition" data-live-search="true" multiple="multiple" title="Palestrante" name="speakers[]">
          <option disabled>Palestrante</option>
          @foreach($speakers as $i)
          <option value="{{ $i->id }}">{{ $i->name }}</option>
          @endforeach
        </select>
        <select class="selectpicker col-md-6" data-style="select-with-transition" data-live-search="true" title="Dia" name="day">
          <option disabled selected>Dia</option>
          @foreach($days as $idx => $i)
          <option value="{{ $i->id }}">Dia {{ $idx+1 }} - {{ $i->date }}</option>
          @endforeach
        </select>
        <div class="col-md-6 mt-2">
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="checkbox" name="confirmed"> Confirmada
              <span class="form-check-sign">
                <span class="check"></span>
              </span>
            </label>
          </div>
        </div>
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary pull-right">Adicionar</button>
          <a href="{{ action('LectureController@index', $event->id) }}" class="btn btn-secondary pull-right">Voltar</a>
        </div>

      </div>
    </form>      
  </div>
</div>
@endsection