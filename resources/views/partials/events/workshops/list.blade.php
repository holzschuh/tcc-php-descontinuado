@extends('partials.events.base')

@section('econtent')
<div class="row">

  <div class="col-md-12">
    <div class="col-md-12 d-flex justify-content-end align-items-center">
      <div class="d-none">
        <h4><strong>Palestras</strong></h4>
      </div>
      <div><a href="{{ action('WorkshopController@create', $event->id) }}" class="btn btn-primary btn-sm btn-round" rel="tooltip" data-original-title="Adicionar nova oficina"><i
            class="material-icons">add</i>
          Incluir</a></div>
    </div>
    <div class="col-md-12">
      @if(session()->has('msg'))
        <p class="alert alert-{{ session('msg.type') }}">{{ session('msg.text') }}</p>                
      @endif

      <table class="table">
        <thead>
          <tr>
            <th>Palestrante</th>
            <th>Nome da oficina</th>
            <th>Limite de ouvintes</th>
            <th>Status</th>
            <th class="th-action"></th>
          </tr>
        </thead>
        <tbody>
        @foreach($list as $o)
          <tr>
            <td>
              @foreach($o->speakers as $sp)
                {{$sp->name}},
              @endforeach
            </td>
            <td><!--small-->{{$o->name}}<!--/small--></td>
            <td>{{$o->limit}}</td>
            <td><span class="badge badge-{{ $o->confirmed ? 'success' : 'danger'}}">{{ $o->confirmed ? 'CONFIRMADO' : 'NÃO CONFIRMADO'}}</span></td>
            <td class="td-action d-flex justify-content-end text-right">
              <!-- <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Ver oficina"><i
                  class="material-icons">visibility</i>
                <div class="ripple-container"></div>
              </a> -->
              <a href="{{ action('WorkshopController@edit', [$event->id, $o])}}" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Editar oficina"><i
                  class="material-icons">edit</i>
                <div class="ripple-container"></div>
              </a>
              <form action="{{ action('WorkshopController@destroy', [$event->id, $o]) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Remover oficina">
                  <i class="material-icons">close</i>
                  <div class="ripple-container"></div>
                </button>
              </form>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <div class="col-md-6 d-none">
    <div class="col-md-12 d-flex justify-content-between align-items-center">
      <div>
        <h4><strong>Oficinas</strong></h4>
      </div>
      <div><a href="#" class="btn btn-primary btn-sm btn-round" rel="tooltip" data-original-title="Adicionar nova oficina"><i
            class="material-icons">add</i>
          Incluir</a></div>
    </div>
    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr>
            <th>Título</th>
            <th>Status</th>
            <th class="th-action"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><small>Lorem ipsum sum da tret bisect growes upl layer</small></td>
            <td><span class="badge badge-success">CONFIRMADO</span></td>
            <td class="td-action text-right">
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Ver oficina"><i
                  class="material-icons">visibility</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Editar oficina"><i
                  class="material-icons">edit</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Remover oficina"><i
                  class="material-icons">close</i>
                <div class="ripple-container"></div>
              </a>
            </td>
          </tr>
          <tr>
            <td><small>Lorem ipsum sum da tret bisect growes upl layer</small></td>
            <td><span class="badge badge-warning">PENDENTE</span></td>
            <td class="td-action text-right">
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Ver oficina"><i
                  class="material-icons">visibility</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Editar oficina"><i
                  class="material-icons">edit</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Remover oficina"><i
                  class="material-icons">close</i>
                <div class="ripple-container"></div>
              </a>
            </td>
          </tr>
          <tr>
            <td><small>Lorem ipsum sum da tret bisect growes upl layer</small></td>
            <td><span class="badge badge-danger">CANCELADO</span></td>
            <td class="td-action text-right">
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Ver oficina"><i
                  class="material-icons">visibility</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Editar oficina"><i
                  class="material-icons">edit</i>
                <div class="ripple-container"></div>
              </a>
              <a href="#" class="btn btn-sm btn-link btn-fab btn-round" rel="tooltip" data-original-title="Remover oficina"><i
                  class="material-icons">close</i>
                <div class="ripple-container"></div>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

</div>
@endsection