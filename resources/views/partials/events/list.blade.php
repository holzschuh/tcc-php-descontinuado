@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex align-items-center"><i class="material-icons pr-1 mb-1">event</i>
                        <h4 class="card-title "> Eventos</h4>
                    </div>
                    <a href="{{ action('EventController@create') }}" class="btn btn-round btn-warning"><i class="material-icons">add</i>Add
                        novo</a>
                </div>
                <!-- <h4 class="card-title ">Administradores <a href="#" class="btn btn-round btn-fab btn-">+</a></h4>
                      <p class="card-category d-none"> Here is a subtitle for this table</p> -->
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if(session()->has('msg'))
                        <p class="alert alert-{{ session('msg.type') }}">{{ session('msg.text') }}</p>                
                    @endif
                    <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Data</th>
                                <th>Local</th>
                                <th><!--Status--></th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($list as $o)
                            <tr>
                                <td>{{ $o->id }}</td>
                            <td><a href="{{ action('EventController@show', $o->id) }}">{{ $o->name }}</a></td>
                                <td>{{ $o->datetime_start }}</td>
                                <td>{{ $o->location }}</td>
                                <td>
                                    <!-- <span class="badge badge-danger">{{ $o->id }}</span> -->
                                </td>
                                <!-- <td class="text-primary">$36,738</td> -->
                                <td class="td-actions ">
                                <a href="{{ action('EventController@edit', $o->id) }}" rel="tooltip" title="" class="btn btn-primary btn-fab btn-round btn-sm"
                                        data-original-title="Editar">
                                        <i class="material-icons">edit</i>
                                        <div class="ripple-container"></div>
                                    </a>
                                    <script>
                                        var abre = function () {
                                            swal({
                                                title: "Feito!",
                                                text: "Evendo encerrado com sucesso!",
                                                buttonsStyling: false,
                                                confirmButtonClass: "btn btn-success",
                                                type: "success"
                                            }).catch(swal.noop)

                                        }
                                    </script>
                                    <button onclick="abre()" type="button" rel="tooltip" title="" class="btn btn-danger btn-fab btn-round btn-sm"
                                        data-original-title="Encerrar">
                                        <i class="material-icons">lock</i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <h2>Nothing</h2>   
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection