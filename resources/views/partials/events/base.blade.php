@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!-- <div class="card-header card-header-primary">
                <h4 class="card-title">NOME DO EVENTO</h4>
              </div> -->
            <div class="card-body">
                

                    <div class="row">
                        <div class="col-md-12 d-flex align-items-center justify-content-start p-2 pl-4">
                            <div>
                                <a href="{{ action('EventController@show', $event->id) }}" rel="tooltip" title="" class="btn btn-light btn-fab btn-round btn-sm" data-original-title="Voltar"><i class="material-icons">arrow_back</i><div class="ripple-container"></div></a>
                            </div>
                        <h3 class="m-0 ml-3"><strong>{{$event->name}}</strong> {{ $breadcrumb }}</h3>
                        </div>
                    </div>

                    {{-- <div class="row">
                        <div class="col">
                            {{-- <hr> 
                        </div>
                    </div> --}}
                
                   @yield('econtent')

                
            </div>
        </div>
    </div>
</div>
@endsection