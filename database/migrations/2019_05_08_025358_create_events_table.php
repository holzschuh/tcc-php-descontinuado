<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->default('untitled');
            $table->string('location', 100);
            $table->datetime('datetime_start')->useCurrent();
            $table->datetime('datetime_end')->useCurrent();
            $table->boolean('finished')->default(false);
            $table->boolean('visible')->default(false);

            //images
            $table->string('logo', 100)->nullable()->default('no-logo.png');
            $table->string('thumbnail', 100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
