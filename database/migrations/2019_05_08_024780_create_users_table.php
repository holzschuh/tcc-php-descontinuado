<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('cpf', 14)->nullable();
            $table->string('bio')->nullable();
            $table->string('password');
            $table->string('photo')->nullable();
          
            $table->bigInteger('role_id')->unsigned()->nullable(); // FIX THAT
            $table->foreign('role_id')->references('id')->on('roles');
            
            $table->bigInteger('institution_id')->unsigned()->nullable()->default(null);
            $table->foreign('institution_id')->references('id')->on('institutions')->onDelete('set null');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
