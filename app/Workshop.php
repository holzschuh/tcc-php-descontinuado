<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    public function speakers()
    {
        return $this->belongsToMany('App\User', 'user_workshop_speakers', 'user_id', 'workshop_id');
    }

    public function day()
    {
        return $this->belongsTo('App\Day');
    }

    public function presences()
    {
        return $this->belongsToMany('App\User', 'user_workshop_roll', 'user_id', 'workshop_id');
    }
}
