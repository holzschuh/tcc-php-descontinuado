<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'cpf',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function institution()
    {
        return $this->belongsTo('App\Institution');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function certificates()
    {
        return $this->hasMany('App\Certificate');
    }

    public function presences()
    {
        return $this->hasMany('App\Presence');
    }
}
