<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    public function speakers()
    {
        return $this->belongsToMany('App\User', 'user_lecture_speakers');
    }

    public function day()
    {
        return $this->belongsTo('App\Day');
    }
}
