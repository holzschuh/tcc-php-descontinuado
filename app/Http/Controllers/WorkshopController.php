<?php

namespace App\Http\Controllers;

use App\Workshop;
use App\Event;
use App\Day;
use Illuminate\Http\Request;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($event)
    {
        $e = Event::findOrFail($event);

        return view('partials.events.workshops.list', [
            'event' => $e, 
            'breadcrumb' => '/ Oficinas',
            'list' => $e->workshops]);
        //return \App\Event::findOrFail($event);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($event)
    {
        $e = Event::findOrFail($event);
        return view('partials.events.workshops.add', [
            'event' => $e, 
            'breadcrumb' => '/ Oficinas / Adicionar',
            'speakers' => \App\User::all(),
            'days' => $e->days()->orderBy('date', 'ASC')->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($event, Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'location' => 'required|max:50',
            'day' => 'required|exists:days,id',
            'speakers' => 'required|exists:users,id',
            'limit' => 'required|min:1',
            'confirmed' => ''
        ]);

        $wshop = new Workshop();
        $wshop->name = $request->get('name');
        $wshop->location = $request->get('location');
        $wshop->limit = $request->get('limit');
        $wshop->day()->associate(Day::find($request->get('day')));
        $wshop->confirmed = $request->get('confirmed') ? true : false;
        
        // $speakers = $request->get('speakers');
        // foreach ($speakers as $speaker) {
        //     $wshop->speakers()->attach(User::find($speaker));
        // }

        $r = $wshop->save();

        $wshop->speakers()->sync($request->get('speakers'));

        //$wshop->save();

        $msg  = $r ? 'Oficina adicionada com sucesso!' : 'Ops! Aconteceu um erro ao adicionar';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('WorkshopController@index', $event)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function show(Workshop $workshop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function edit($event, Workshop $workshop)
    {
        $e = Event::findOrFail($event);
        return view('partials.events.workshops.edit', [
            'event' => $e,
            'o' => $workshop, 
            'breadcrumb' => '/ Oficinas / Editar',
            'speakers' => \App\User::all(),
            'days' => $e->days()->orderBy('date', 'ASC')->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function update($event, Request $request, Workshop $workshop)
    {
        $request->validate([
            'name' => 'required|max:100',
            'location' => 'required|max:50',
            'day' => 'required|exists:days,id',
            'speakers' => 'required|exists:users,id',
            'limit' => 'required|min:1',
            'confirmed' => ''
        ]);

        $wshop = $workshop;
        $wshop->name = $request->get('name');
        $wshop->location = $request->get('location');
        $wshop->limit = $request->get('limit');
        $wshop->day()->associate(Day::find($request->get('day')));
        $wshop->confirmed = $request->get('confirmed') ? true : false;
        
        // $speakers = $request->get('speakers');
        // foreach ($speakers as $speaker) {
        //     $wshop->speakers()->attach(User::find($speaker));
        // }

        $r = $wshop->save();

        $wshop->speakers()->sync($request->get('speakers'));

        $msg  = $r ? 'Oficina atualizada com sucesso!' : 'Ops! Aconteceu um erro ao atualizar';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('WorkshopController@index', $event)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function destroy($event, Workshop $workshop)
    {
        $r = $workshop->delete();
        
        $msg  = $r ? 'Ofinica removida com sucesso!' : 'Ops! Aconteceu um erro ao deletar';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('WorkshopController@index', $event)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }
}
