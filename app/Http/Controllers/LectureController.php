<?php

namespace App\Http\Controllers;

use App\Lecture;
use App\Event;
use App\User;
use App\Day;
use Illuminate\Http\Request;

class LectureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($event)
    {
        $e = Event::findOrFail($event);

        return view('partials.events.lectures.list', [
            'event' => $e, 
            'breadcrumb' => '/ Palestras',
            'list' => $e->lectures]);
        // return response()->json(['Event' => $event], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($event)
    {
        $e = Event::findOrFail($event);
        return view('partials.events.lectures.add', [
            'event' => $e, 
            'breadcrumb' => '/ Palestras / Adicionar',
            'speakers' => \App\User::all(),
            'days' => $e->days()->orderBy('date', 'ASC')->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($event, Request $request)
    {
        // return Lecture::with(array('speakers' => function($q){
        //     return $q->select('name');
        // }))->get();
        
        $request->validate([
            'name' => 'required|max:100',
            'location' => 'required|max:50',
            'day' => 'required|exists:days,id',
            'speakers' => 'required|exists:users,id',
            'confirmed' => ''
        ]);

        $lec = new Lecture();
        $lec->name = $request->get('name');
        $lec->location = $request->get('location');
        $lec->day()->associate(Day::find($request->get('day')));
        $lec->confirmed = $request->get('confirmed') ? true : false;
        
        // $speakers = $request->get('speakers');
        // foreach ($speakers as $speaker) {
        //     $lec->speakers()->attach(User::find($speaker));
        // }

        $r = $lec->save();

        $lec->speakers()->sync($request->get('speakers'));

        //$lec->save();

        $msg  = $r ? 'Palestra adicionada com sucesso!' : 'Ops! Aconteceu um erro ao adicionar';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('LectureController@index', $event)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lecture  $lecture
     * @return \Illuminate\Http\Response
     */
    public function show($event, Lecture $lecture)
    {
        return response()->json(['where'=>'Create','Event' => $event], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lecture  $lecture
     * @return \Illuminate\Http\Response
     */
    public function edit($event, Lecture $lecture)
    {
        $e = Event::findOrFail($event);
        return view('partials.events.lectures.edit', [
            'event' => $e,
            'o' => $lecture, 
            'breadcrumb' => '/ Palestras / Editar',
            'speakers' => \App\User::all(),
            'days' => $e->days()->orderBy('date', 'ASC')->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lecture  $lecture
     * @return \Illuminate\Http\Response
     */
    public function update($event, Request $request, Lecture $lecture)
    {
        $request->validate([
            'name' => 'required|max:100',
            'location' => 'required|max:50',
            'day' => 'required|exists:days,id',
            'speakers' => 'required|exists:users,id',
            'confirmed' => ''
        ]);

        $lec = $lecture;
        $lec->name = $request->get('name');
        $lec->location = $request->get('location');
        $lec->day()->associate(Day::find($request->get('day')));
        $lec->confirmed = $request->get('confirmed') ? true : false;
        
        // $speakers = $request->get('speakers');
        // foreach ($speakers as $speaker) {
        //     $lec->speakers()->attach(User::find($speaker));
        // }

        $r = $lec->save();

        $lec->speakers()->sync($request->get('speakers'));

        $msg  = $r ? 'Palestra atualizada com sucesso!' : 'Ops! Aconteceu um erro ao deletar';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('LectureController@index', $event)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lecture  $lecture
     * @return \Illuminate\Http\Response
     */
    public function destroy($event, Lecture $lecture)
    {
        $r = $lecture->delete();
        
        $msg  = $r ? 'Palestra removida com sucesso!' : 'Ops! Aconteceu um erro ao deletar';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('LectureController@index', $event)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }
}
