<?php

namespace App\Http\Controllers;

use App\Day;
use App\Event;
use Illuminate\Http\Request;

class DayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($event)
    {
        $e = Event::findOrFail($event);

        return view('partials.events.days.list', [
            'event' => $e, 
            'list'  => $e->days()->orderBy('date', 'ASC')->get(), 
            'breadcrumb' => '/ Dias'
        ]);
        //return \App\Event::findOrFail($event);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($event)
    {
        return view('partials.events.days.add', ['event' => Event::findOrFail($event), 'breadcrumb' => '/ Dias / Adicionar']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($event, Request $request)
    {
        $e = Event::findOrFail($event);
        $request->validate([
            'event' => 'required|exists:events,id',
            'datetime_day' => 'required|after_or_equal:'.$e->datetime_start.'|before_or_equal:'.$e->datetime_end,
        ]);

        $day = new Day();
        $day->date = $request->get('datetime_day');
        $day->event()->associate(Event::find($request->get('event')));

        $day->save();

        $msg  = 'Dia adicionado com sucesso!';
        $type = 'success';

        return redirect()->action('DayController@index', $day->event->id)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function show(Day $day)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function edit(Day $day)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Day $day)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function destroy($event, Day $day)
    {
        $r = $day->delete();
        
        $msg  = $r ? 'Dia removido com sucesso!' : 'Ops! Aconteceu um erro ao deletar';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('DayController@index', $day->event->id)->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }
}
