<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partials.events.list', ['list' => Event::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partials.events.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'location' => 'required|max:100',
            'datetime_start' => 'required',
            'datetime_end' => 'required',
        ]);

        $event = new Event([
            'name' => $request->get('name'),
            'location' => $request->get('location'),
            'datetime_start' => $request->get('datetime_start'),
            'datetime_end' => $request->get('datetime_end')
        ]);

        $event->save();

        return response()->redirectToRoute('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('partials.events.view', ['obj' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('partials.events.edit', ['obj' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $request->validate([
            'name' => 'required|max:100',
            'location' => 'required|max:100',
            'datetime_start' => 'required',
            'datetime_end' => 'required',
        ]);

        $event->name = $request->get('name');
        $event->location = $request->get('location');
        $event->datetime_start = $request->get('datetime_start');
        $event->datetime_end = $request->get('datetime_end');
        
        $event->save();

        return response()->redirectToRoute('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
