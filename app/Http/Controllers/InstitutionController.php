<?php

namespace App\Http\Controllers;

use App\Institution;
use Illuminate\Http\Request;

class InstitutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partials.institutions.list', ['list' => Institution::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partials.institutions.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:60'
        ]);

        $institution = new Institution([
            'name' => $request->get('name')
        ]);

        $institution->save();

        return redirect()->route('institutions.index');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function show(Institution $institution)
    {
        return Institution::findOrFail($institution->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function edit(Institution $institution)
    {
        return view('partials.institutions.edit', ['obj' => $institution]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Institution $institution)
    {
        $request->validate([
            'name' => 'required|max:60'
        ]);

        $institution->name = $request->get('name');
        $institution->save();

        $msg  = 'Instituição atualizada com sucesso!';
        $type = 'success';

        return redirect()->action('InstitutionController@index')->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institution $institution)
    {
        $r = $institution->delete();
        $msg  = 'Instituição removida com sucesso!';
        $type = 'success';

        return redirect()->action('InstitutionController@index')->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }
}
