<?php

namespace App\Http\Controllers;

use App\User;
use App\Institution;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partials.users.list', ['list' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partials.users.add', ['institutions' => Institution::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:60',
            'email' => 'required|email',
            'cpf' => 'max:14',
            'institution' => 'required|exists:institutions,id'
        ]);
        
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->cpf = $request->get('cpf');
        $user->password = md5('123456');
        $user->bio = $request->get('bio');
        $user->institution()->associate(Institution::find($request->get('institution')));
        $r = $user->save();

        $msg  = $r ? 'Usuário atualizado com sucesso!' : 'Ops! Ocorreu um erro';
        $type = $r ? 'success' : 'danger';

        return redirect()->action('UserController@index')->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('partials.users.edit', ['obj' => $user, 'institutions' => Institution::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|max:60',
            'email' => 'required|email',
            'cpf' => 'max:14',
            'institution' => 'required|exists:institutions,id'
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->cpf = $request->get('cpf');
        $user->bio = $request->get('bio');
        $user->institution()->associate(Institution::find($request->get('institution')));
        $user->save();

        $msg  = 'Usuário atualizado com sucesso!';
        $type = 'success';

        return redirect()->action('UserController@index')->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $r = $user->delete();
        $msg  = 'Usuário removido com sucesso!';
        $type = 'success';

        return redirect()->action('UserController@index')->with([
            'msg'=>[
                'text' => $msg, 
                'type' => $type
            ]
        ]);
    }
}
