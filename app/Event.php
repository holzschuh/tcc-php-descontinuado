<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'location', 'datetime_start', 'datetime_end', 'logo', 'thumbnail',
    ];

    protected $dates = [
        'datetime_start',
        'datetime_end'
    ];

    public function days()
    {
        return $this->hasMany('App\Day');
    }

    public function certificates()
    {
        return $this->hasMany('App\Certificate');
    }

    public function enrollments()
    {
        return $this->hasMany('App\Enrollment');
    }

    public function lectures()
    {
        return $this->hasManyThrough('App\Lecture', 'App\Day');
    }

    public function workshops()
    {
        return $this->hasManyThrough('App\Workshop', 'App\Day');
    }
}
